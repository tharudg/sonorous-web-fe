import { Component, OnInit, HostListener } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  @HostListener("window:scroll", ["$event"])
  onWindowScroll(e) {
    if (window.pageYOffset > 5) {
      let element = document.getElementById("header");
      element.classList.add("header--scrolled");
    } else {
      let element = document.getElementById("header");
      element.classList.remove("header--scrolled");
    }
  }
}

import { Component, OnInit } from "@angular/core";
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder
} from "@angular/forms";

@Component({
  selector: "app-contact-form",
  templateUrl: "./contact-form.component.html",
  styleUrls: ["./contact-form.component.scss"]
})
export class ContactFormComponent implements OnInit {
  contactForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.contactForm = this.formBuilder.group({
      firstName: new FormControl("", [
        Validators.required,
        Validators.minLength(3)
      ]),
      lastName: new FormControl("", [
        Validators.required,
        Validators.minLength(3)
      ]),
      email: new FormControl("", [Validators.required, Validators.email]),
      problemFacing: new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ]),
      message: new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ])
    });
  }

  get formElement() {
    return this.contactForm.controls;
  }

  send() {
    console.log(this.contactForm.value);
    if (this.contactForm.invalid) {
      return;
    }
  }
  ngOnInit() {}
}

import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { ParticlesModule } from "angular-particle";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material.module";
import { HeaderComponent } from "./components/header/header.component";
import { FooterComponent } from "./components/footer/footer.component";
import { HomeComponent } from "./pages/home/home.component";
import { AboutComponent } from "./pages/about/about.component";
import { OurServicesComponent } from "./pages/our-services/our-services.component";
import { PageNotFoundComponent } from "./pages/page-not-found/page-not-found.component";
import { ComingSoonComponent } from "./components/coming-soon/coming-soon.component";
import { MainContainerComponent } from "./pages/main-container/main-container.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ContactFormComponent } from "./components/contact-form/contact-form.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    OurServicesComponent,
    PageNotFoundComponent,
    ComingSoonComponent,
    MainContainerComponent,
    ContactFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ParticlesModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

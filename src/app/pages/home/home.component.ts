import { Component, OnInit } from '@angular/core';
import {ParticlesConfig} from './particleConfig'
import {MatIconModule} from '@angular/material/icon';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  width: number = 100;
  height: number = 100;
  style;
  params;
  i=0;

  feedBacks=[{img: "https://media.zigcdn.com/media/model/2019/Sep/pulsarns160-right-side-view_360x240.jpg",
              name:'Sudesh Gunasekara',
              designation:'Trainner',
              comment:"kgwuyflw iuggggggggggggg ggggggggggggeuwgduiedgwgedlwehueugeog"              
},
{img: 'https://auto.ndtvimg.com/bike-images/colors/hero/xpulse-200/hero-xpulse-200-matte-green.png?v=1556777878',
              name:'Gunasekara',
              designation:'Trainner',
              comment:"kgwuyflwi ugeuwgdui edgwgedlwehueugeog"              
},
{img: 'https://www.motorbeam.com/wp-content/uploads/KTM-Duke-200-BS6.jpg',
              name:'Sudesh',
              designation:'Trainner',
              comment:"kgwuyddddd dddddddddddddddddd dddddddddddflwiugeuwgduiedgwgedlwehueugeog"              
},
{img: 'https://wallpaperaccess.com/full/1890484.jpg',
              name:'Kumara Gunasekara',
              designation:'Trainner',
              comment:"kgwuyflwi ugeuwgdbbbbbbbbbbbbbuiedgwgedlwehueugeog"              
},

]


  constructor() {
    this.params = ParticlesConfig
    this.style ={
      'position': 'fixed',
      'z-index': 5,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
  };
   }

  ngOnInit() {
   
  }
  next(){
   
    if(this.i>=this.feedBacks.length-1){
        this.i =0;   
    }
    else{
      this.i ++
    }
  }
  previous(){
    if(this.i<=0){
      this.i = this.feedBacks.length-1
      
    }
    else{this.i --}
  }
}

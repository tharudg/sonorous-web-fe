import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { OurServicesComponent } from "./pages/our-services/our-services.component";
import { AboutComponent } from "./pages/about/about.component";
import { PageNotFoundComponent } from "./pages/page-not-found/page-not-found.component";
import { ComingSoonComponent } from "./components/coming-soon/coming-soon.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "our-services", component: OurServicesComponent },
  { path: "about", component: AboutComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
